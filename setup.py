from setuptools import setup

setup(name='emv_test',
      version='0.1',
      description='simples example in the python world',
      url='https://gitlab.com/manish.jaggi/py_docker_test.git',
      author='manish',
      author_email='manish.jaggi@emvirt.com',
      license='MIT',
      packages=['emv_test'],
)
